import {Navbar, Nav} from 'react-bootstrap'
import Link from 'next/link'

//create a function that will describe the structure of this component
export default function NavBar() {
	return (
		<Navbar bg="info" expand="lg" variant="dark" className="p-1">
			<Link href="/">
				<a className="navbar-brand">COVID-19 tracker</a>
			</Link>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
					{/*top infected countries with covid-19*/}
					<Link href="/covid/top">
						<a className="nav-link">Top Countries</a>
					</Link>
					{/*find a country to see number of covid-19 cases*/}
					<Link href="/covid/search">
						<a className="nav-link">Find a country</a>
					</Link>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)

}