//we will create a logic that will convert the string API result to numeric data types

//we want to use this function outside
export default function toNum (str) {
	//conver the string to an array to gain access to array methods
	let arr = [...str]
	//visualize = ['','','']
	//let's filter the commas in the string
	const elementNaWalangComma = arr.filter(element => element !== ',')
	//visualize ['' '' '']
	//reduce the filtered array back into a single string

	//this string can now be parsed into a number via the paseInt method
	return parseInt(elementNaWalangComma.reduce((x, y) => x + y))
}