import {useState} from 'react';
import {Form, Button, Alert} from 'react-bootstrap'
import Head from 'next/head'
import DoughnutChart from '../../components/DoughnutChart'
import toNum from '../../helpers/toNum'
//let's initialize a state hook to bind our form components with their respective values.

export default function Search ({newData}) {

	console.log(newData)

	//let's get the status of the coutnries by acquiring the countries_stat array which holds the collection/records of each country
	const countryCollection = newData.countries_stat
	console.log(countryCollection)

	//let's create 3 components that will describe the information that we will acquire from the records
	const [deathCount, setDeathCount] = useState(0)
	const [criticalCount, setCriticalCount] = useState(0)
	const [recoveryCount, setRecoveryCount] = useState(0)

	const [targetCountry, setTargetCountry] = useState("")

	const [name, setName]= useState("")

	//let's create a function that will query to search for a specific country inside our records
	const search = (e) => {
		e.preventDefault() //to avoid page redirection

		const countryMatch = countryCollection.find(bansa => bansa.country_name.toLowerCase() === targetCountry.toLowerCase())
		//there are 2 possible scenarios that will come from the search query (success, not exist)
		if (!countryMatch || countryMatch === null || countryMatch === 'undefined') {
			alert('country does not exist, use another name')
			setName("")
			setTargetCountry("")
		} else {
			console.log(countryMatch)
			setName(countryMatch.country_name)
			//before we feed/inject the data inside our diagram, the information must be converted into numerical type first
			setDeathCount(toNum(countryMatch.deaths)) 
			setCriticalCount(toNum(countryMatch.serious_critical)) 
			setRecoveryCount(toNum(countryMatch.total_recovered))
		}
	}

	return (
		<>
			<Head>
				<title>
				COVID-19 Country Search
				</title>
			</Head>
			<Form onSubmit={e => search(e)}>
				<Form.Label className="mt-3">Country</Form.Label>
				<Form.Group controlId="country">
				<Form.Control type="text" placeholder="search country here" className="mt-3 mb-3" value={targetCountry} onChange={e => setTargetCountry(e.target.value)}/>
				</Form.Group>
				<Button className="mb-3" bg="info" type="submit">search for country</Button>
			</Form>

		{name !== '' ? 
			<>
				<h3>Country: {name}</h3>
				<DoughnutChart criticals={criticalCount} deaths={deathCount} recoveries={recoveryCount}/>
			</>
			:
			<>
				<Alert variant="info">search for a country using the name</Alert>
			</>
		} 
			</>
		)
}

export async function getStaticProps () {
	//fetch the data from our api documentation.
	const fetchedResponse = await fetch('https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php', {
		'method': 'GET',
		'headers': {
			//identifies who is the author of the api from our CMS
			'x-rapidapi-host': 'coronavirus-monitor.p.rapidapi.com',
			//identify the authorization key that will grant us access to the information provided by the documentation
			'x-rapidapi-key': '6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539'
		}
	})

	const newData = await fetchedResponse.json()
	return {
		props: {
			newData
		}
	}
}
