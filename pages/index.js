import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div>
      <Head>
        <title>covid tracker app</title>
        <link rel="icon" href="/favicon.ico"/>
      </Head>
        {/*create own landing page by providing the design*/}
    </div>
  )
}
